
# Програма обчислює дерево можливих ходів та оцінює результат гри для гравця, зробившого перший хід.
# Дошка зображена у вигляді двох бінарних чисел довжиною в кількість клітинок дошки.
# Одне число описує стан дошки з білими шашками, друге з чорними. Одиниця відповідає наявності на клітинці шашки.
# В програмі можна задавати розміри ігрового поля. На прикладі дошки стандартного розміру із 7 стовпчиків та 6 рядів
# розглянемо зображення дошки бінарним числом. На схемі зображено дошку з прономерованими клітинками, номера яких
# відповідають номерам бітів числа в бінарному вигляді. Пуста дошка опишеться числом:
# біт під номером 41--> 000000 000000 000000 000000 000000 000000 000000 --> біт під номером 0.
#       ____ ____ ____ ____ ____ ____ ____
#      |    |    |    |    |    |    |    |
#      | 5  | 11 | 17 | 23 | 29 | 35 | 41 |
#      |____|____|____|____|____|____|____|
#      |    |    |    |    |    |    |    |
#      | 4  | 10 | 16 | 22 | 28 | 34 | 40 |
#      |____|____|____|____|____|____|____|
#      |    |    |    |    |    |    |    |
#      | 3  | 9  | 15 | 21 | 27 | 33 | 39 |
#      |____|____|____|____|____|____|____|
#      |    |    |    |    |    |    |    |
#      | 2  | 8  | 14 | 20 | 26 | 32 | 38 |
#      |____|____|____|____|____|____|____|
#      |    |    |    |    |    |    |    |
#      | 1  | 7  | 13 | 19 | 25 | 31 | 37 |
#      |____|____|____|____|____|____|____|
#      |    |    |    |    |    |    |    |
#      | 0  | 6  | 12 | 18 | 24 | 30 | 36 |
#      |____|____|____|____|____|____|____|
#
# Функція make_empty_board() чисто технічна, формує пусте ігрове поле.
# Функція make_start_board(empty_board) також технічна (дані можна задати вручну) формує тупль, містить чотири елементи
# статус позиції білих, статус позиції чорних, чий хід наступний, маску ходу (число).
# Функція get_possible_moves(position) визначає можливі наступні ходи, та маски ходів (клітинка на яку було зроблено
# хід) буде містити одиницю, а решта бітів нулі.
# Функція generate_list_of_masks (directions) генерує маски виграшних комбінацій. Має певні обмеження через те, що
# поле дошки може бути менше ніж дожина виграшної комбінації (чотири в ряд).
# Функція generate_complete_list_of_masks(start_masks) генерує список всіх масок виграшних комбінацій в рамках
# поля заданої величини.
# Функція create_mapping(masks) створює словник, в якому ключами являються числа, що відповідають кожній окремій
# клітинці дошки, а значеннями всі маски виграшних позицій, які належать даній клітинці.
# Функція evaluation_of_position(position, dict_masks) оцінює статус ходу: невідомо, програш, нічия. Вона накладає
# маски виграшних комбінацій на маску дошки (відповідно білої, або чорної позицій) взявши їх зі словника масок,
# по ключу який відповідає клітинці на яку зроблений хід.
# Функція solve(tree, position, dict_masks) власне рекурсивно будує дерево обчислень.
# Функція print_board(board) чисто технічна друкує позиція за заданими даними для спрощення візуалізації процесу.
# Використовується при відладці програми.
# Для дошки 4*4 заповнення всієї дошки (тобто логічне або між білою і чорною позиціями) виглядає так:
# для позиції (34902208,1158176769,1,4)-> (позиція білих, позиція чорних, хід чорних, маска ходу)
#
#   0  1  1  0
#   1  1  1  0
#   1  1  1  1
#   1  1  1  1


ROW = 4
COLUMN = 5
LENGTH_OF_WINNING_ROW = 4
WHITE_MOVES = 0
BLACK_MOVES = 1
WIN_FOR_MOVING_SIDE = 'win'
LOSS_FOR_MOVING_SIDE = 'loss'
UNKNOWN_FOR_MOVING_SIDE = 'unknown'
DRAW = 'draw'
FULL_BOARD = ((0b1 << (ROW * COLUMN))-1)
COLUMN_MARGIN = COLUMN - LENGTH_OF_WINNING_ROW
ROW_MARGIN = ROW - LENGTH_OF_WINNING_ROW

DIRECTIONS = (
    (0b0001,(ROW + 1),(COLUMN_MARGIN + 1, ROW_MARGIN, COLUMN_MARGIN)),
    (0b1000,(ROW-1),  (COLUMN_MARGIN + 1, ROW_MARGIN, COLUMN_MARGIN)),
    (0b0001, ROW,     ((ROW * (COLUMN_MARGIN + 1))-1, 1, None)),
    (0b0001, 1,       (COLUMN, ROW_MARGIN, COLUMN -1))
             )


def make_empty_board():
    board = (0b0)
    return board


def make_start_board(empty_board):
    mask_for_new_move = 0b0
    who_moves = WHITE_MOVES
    start_board = (empty_board, empty_board, who_moves, mask_for_new_move)
    return start_board


def is_stalemate(white_board, black_board):
    return FULL_BOARD == white_board | black_board


def get_possible_moves(position):
    result = []
    white_board, black_board, who_moves = position[:3]
    mask_of_board = white_board | black_board
    if is_stalemate(white_board, black_board):
        return result
    full_collumn = (1 << ROW)-1
    for column in range(COLUMN):
        checkers_in_column = (mask_of_board >> (ROW * column)) & full_collumn
        if checkers_in_column == full_collumn:
            continue
        mask_for_new_move = checkers_in_column + 1
        mask_for_new_move <<= ROW * column
        new_board_in_position = position[who_moves] | mask_for_new_move
        result.append((new_board_in_position, black_board, BLACK_MOVES, mask_for_new_move) if who_moves ==
                        WHITE_MOVES else (white_board, new_board_in_position, WHITE_MOVES, mask_for_new_move))
    return result


def generate_list_of_masks (directions):
    list_of_masks = []
    if ROW < LENGTH_OF_WINNING_ROW and COLUMN < LENGTH_OF_WINNING_ROW:
            return list_of_masks
    for bit, shift, operation in directions:
        for i in range(LENGTH_OF_WINNING_ROW - 1):
            bit = (bit << shift) | bit
        if (ROW < LENGTH_OF_WINNING_ROW and shift == ROW) or\
            (COLUMN < LENGTH_OF_WINNING_ROW and shift == 1) or\
            (ROW >= LENGTH_OF_WINNING_ROW and COLUMN >= LENGTH_OF_WINNING_ROW):
                list_of_masks.append((bit,operation))
    return list_of_masks


def generate_complete_list_of_masks(start_masks):
    result = []
    for mask,operation in start_masks:
        result.append(mask)
        for i in range(operation[0]):
            for k in range(operation[1]):
                mask <<= 1
                result.append(mask)
            if operation[2] is not None and i != operation[2]:
                mask <<= LENGTH_OF_WINNING_ROW
                result.append(mask)
    return result


def create_mapping(masks):
    mask_saver = {}
    square_for_check = 0b1
    for square in range(ROW * COLUMN):
        masks_for_square = []
        for mask in masks:
            if square_for_check & mask:
                masks_for_square.append(mask)
        mask_saver[square_for_check] = masks_for_square
        square_for_check <<= 1
    return mask_saver


def evaluation_of_position(position, dict_masks):
    white_board, black_board, who_next_moves, mask_of_move = position
    if mask_of_move not in dict_masks:
        return UNKNOWN_FOR_MOVING_SIDE
    masks_of_evaluating = dict_masks[mask_of_move]
    board_for_evaluation = white_board if who_next_moves == BLACK_MOVES else black_board
    for mask in masks_of_evaluating:
        if board_for_evaluation & mask == mask:
            return LOSS_FOR_MOVING_SIDE
    if not is_stalemate(white_board, black_board):
        return UNKNOWN_FOR_MOVING_SIDE
    return DRAW


from time import monotonic as time
start=time()

def solve(tree, position, dict_masks):
    if not (len(tree)%10000):
        print(int(time()-start), len(tree))
    key_for_tree = position[0:3]
    win_loss = tree.get(key_for_tree)
    if win_loss:
        return tree, win_loss
    tree[key_for_tree] = evaluation_of_position(position, dict_masks)
    if tree[key_for_tree] == LOSS_FOR_MOVING_SIDE or tree[key_for_tree] == DRAW:
        return tree,tree[key_for_tree]
    positions = get_possible_moves(position)
    outcomes = (LOSS_FOR_MOVING_SIDE, DRAW, WIN_FOR_MOVING_SIDE)
    item_of_outcome = -1
    for new_position in positions:
        tree, win_loss = solve(tree, new_position, dict_masks)
        if win_loss == LOSS_FOR_MOVING_SIDE:
            position_item_of_outcome = 2
        elif win_loss == DRAW:
            position_item_of_outcome = 1
        else:
            position_item_of_outcome = 0
        if position_item_of_outcome >= item_of_outcome:
            item_of_outcome = position_item_of_outcome
    tree[key_for_tree] = outcomes[item_of_outcome]
    return tree, tree[key_for_tree]


def print_board(board):
    board = (bin(board)[2:]).zfill(ROW * COLUMN)[::-1]
    for i in range(ROW):
        number_token = ROW-1-i
        for k in range(COLUMN):
            print(board[number_token], ' ', end='')
            number_token += ROW
        print('\n')



list_of_masks = generate_list_of_masks(DIRECTIONS)
masks = generate_complete_list_of_masks(list_of_masks)
dict_masks = create_mapping(masks)
board = make_empty_board()
start_position = make_start_board(board)
result, status = solve({}, start_position, dict_masks)
print(result)
print(len(result))
print(status)

