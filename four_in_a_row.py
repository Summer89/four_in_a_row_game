
ROW = 2
COLUMN = 1
WHITE_CHECKER = 'w'
BLACK_CHECKER = 'b'
WHITE_MOVES = 'W'
BLACK_MOVES = 'B'
WIN_FOR_MOVING_SIDE = 'win'
LOSS_FOR_MOVING_SIDE = 'loss'
UNKNOWN_FOR_MOVING_SIDE = 'unknown'
DRAW = 'draw'
EMPTY_SQUARE = 'o'
DIRECTIONS = ((1, 1), (-1, 1), (0, 1), (1, 0))


def make_empty_board(column, row):
    board = [[EMPTY_SQUARE]*row for _ in range(column)]
    return board


def make_start_board(empty_board):
    start_board = [empty_board, WHITE_MOVES]
    return start_board


def position_to_string(position):
    return (''.join(
        ''.join(row) for row in position[0])
           ) + position[1]


def string_to_position(string, position=make_empty_board(COLUMN, ROW)):
    index = 0
    for row in position:
        for square in range(len(row)):
            row[square] = string[index]
            index += 1
    return [position, string[index]]


def get_possible_moves(string_of_position):
    result = []
    position = string_to_position(string_of_position)
    board = position[0]
    who_moves = position[1]
    for column in range(len(board)):
        for square in range(len(board[column])):
            if board[column][square] == EMPTY_SQUARE:
                board[column][square] = who_moves.lower()
                result.append(position_to_string([board,WHITE_MOVES if who_moves == BLACK_MOVES else BLACK_MOVES]))
                board[column][square] = EMPTY_SQUARE
                break
    return result


def in_board(x,y):
    return 0<=x<COLUMN and 0<=y<ROW


def list_of_coordinate(x,y):
    result = []
    for i in range(len(DIRECTIONS)):
        new_x,new_y = x,y
        new_coordinates = []
        for k in range(3):
            (dx,dy) = DIRECTIONS[i]
            if in_board(new_x+dx,new_y+dy):
                coordinates = [new_x+dx, new_y+dy]
                new_coordinates.append(coordinates)
                new_x,new_y = coordinates[0],coordinates[1]
        if new_coordinates != [] and len(new_coordinates) == 3:
            result.append(new_coordinates)
    return result


def evaluation_of_position(string_of_position):
    position = string_to_position(string_of_position)
    board = position[0]
    for x in range(len(board)):
        for y in range(len(board[x])):
            if board[x][y] != EMPTY_SQUARE:
                valuations = list_of_coordinate(x, y)
                for list in range(len(valuations)):
                    counter = 1
                    for i in range(len(valuations[list])):
                        new_x, new_y = valuations[list][i]
                        counter += 1 if board[x][y] == board[new_x][new_y] else 0
                        if counter == 4:
                            return LOSS_FOR_MOVING_SIDE
    if get_possible_moves(string_of_position):
        return UNKNOWN_FOR_MOVING_SIDE
    return DRAW

from time import monotonic as time
start=time()

def solve(tree, position):
    if not (len(tree) % 10000):
        print(int(time()-start), len(tree))
    win_loss = tree.get(position)
    if win_loss:
        return tree, win_loss
    tree[position] = evaluation_of_position(position)
    if tree[position] == LOSS_FOR_MOVING_SIDE or tree[position] == DRAW:
        return tree,tree[position]
    positions = get_possible_moves(position)
    outcomes = (LOSS_FOR_MOVING_SIDE, DRAW, WIN_FOR_MOVING_SIDE)
    item_of_outcome = -1
    for new_position in positions:
        tree, win_loss = solve(tree, new_position)
        if win_loss == LOSS_FOR_MOVING_SIDE:
            position_item_of_outcome = 2
        elif win_loss == DRAW:
            position_item_of_outcome = 1
        else:
            position_item_of_outcome = 0
        if position_item_of_outcome >= item_of_outcome:
            item_of_outcome = position_item_of_outcome
    tree[position] = outcomes[item_of_outcome]
    return tree, tree[position]





board = make_empty_board(COLUMN, ROW)
board = make_start_board(board)
string = position_to_string(board)
result, status = solve({},string)
print(result)
print(len(result))
print(status)


